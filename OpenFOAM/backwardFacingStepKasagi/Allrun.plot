#!/bin/sh
GNUPLOT=$(which gnuplot 2> /dev/null)
if [ "x$GNUPLOT" = "x" ]
then
    echo "Gnuplot not installed" >&2
    exit 1
fi

for file in share/plot/*.gp
do
  $GNUPLOT $file 
done

epsfiles="residual.eps profileU.eps profileV.eps profilek.eps"
base=`basename ${PWD}`
CONVERT=$(which convert 2> /dev/null)
if [ ! "x$CONVERT" = "x" ]
then
    $CONVERT -density 300 $epsfiles $base.pdf
else
    echo "convert not installed" >&2
fi
