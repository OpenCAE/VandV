#!/bin/sh
(
    for i in northWall southWall eastWall westWall northRoof southRoof
    do
	paste plot/expt/${i}.txt postProcessing/sample/*/targetHouse_${i}_wpc.xy
	echo
	echo
    done
) | awk \
'/^./ {n++;se+=($4-$8)**2} {print $0} END {print "#",sqrt(se/n)}' > corwpc.txt
gnuplot plot/corwpc.gp
gnuplot plot/residual.gp
