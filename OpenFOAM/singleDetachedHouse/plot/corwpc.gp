set terminal pdf font "Arial,16" 
set style data points
set xlabel "Experiment"
set ylabel "CFD"
set xrange [-1.2:1.2]
set yrange [-1.2:1.2]
set xtics 0.4
set ytics 0.4
set mxtics 4
set mytics 4
set size square 1,1
set xzeroaxis
set yzeroaxis
set key outside box
set pointsize 0.5
set grid


set output "corwpc.pdf"
rmse=`awk '/^#/ {print $2}' corwpc.txt`
set label sprintf("RMSE: %.4f",rmse) at 1.7,0
plot \
x title "" \
,"corwpc.txt" index 0 using 4:8 title "Wall(N)" with p pt 1 lw 1 \
,"corwpc.txt" index 1 using 4:8 title "Wall(S)"  with p pt 2 lw 1 \
,"corwpc.txt" index 2 using 4:8 title "Wall(E)"  with p pt 4 lw 1 \
,"corwpc.txt" index 3 using 4:8 title "Wall(W)"  with p pt 6 lw 1 \
,"corwpc.txt" index 4 using 4:8 title "Roof(N)" with p pt 8 lw 1 \
,"corwpc.txt" index 5 using 4:8 title "Roof(S)" with p pt 10 lw 1
