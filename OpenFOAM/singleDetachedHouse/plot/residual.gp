set terminal pdf font "Arial,16"
set output "residual.pdf"
set logscale y
set xlabel "Time"
set ylabel "Residual"
set grid
set style data line
plot \
"postProcessing/solverInfo/0/solverInfo.dat" \
   using 1:3  title "Ux",\
"" using 1:6  title "Uy",\
"" using 1:9  title "Uz",\
"" using 1:14  title "k",\
"" using 1:19 title "p",\
"" using 1:24  title "epsilon"
#    EOF
