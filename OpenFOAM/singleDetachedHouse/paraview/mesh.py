# state file generated using paraview version 5.11.0
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [2146, 1244]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [-0.04320000112056732, 0.016792314537161235, 0.060567005776680546]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.25130129044504873, 0.15213143998916573, 0.21979832494836146]
renderView1.CameraFocalPoint = [3.3478923805072847, -2.333354465426415, -2.6218233385871814]
renderView1.CameraViewUp = [0.5646804001799314, -0.1164244910292106, 0.8170565363187678]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.3499999642372131
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(2146, 1244)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.MeshRegions = ['internalMesh', 'patch/_x', 'patch/_y', 'patch/_z', 'patch/targetHouse_eastUnderEaves', 'patch/targetHouse_eastWall', 'patch/targetHouse_northRoof', 'patch/targetHouse_northRoofEnd', 'patch/targetHouse_northUnderEaves', 'patch/targetHouse_northWall', 'patch/targetHouse_southRoof', 'patch/targetHouse_southRoofEnd', 'patch/targetHouse_southUnderEaves', 'patch/targetHouse_southWall', 'patch/targetHouse_westUnderEaves', 'patch/targetHouse_westWall', 'patch/x_', 'patch/y_', 'patch/z_']
pvfoam.Cachemesh = 0
pvfoam.Decomposepolyhedra = 0

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.Selectors = ['/Root/boundary/_y', '/Root/boundary/z_', '/Root/boundary/targetHouse_eastWall', '/Root/boundary/targetHouse_northWall', '/Root/boundary/targetHouse_southWall', '/Root/boundary/targetHouse_westWall', '/Root/boundary/targetHouse_northRoof', '/Root/boundary/targetHouse_southRoof', '/Root/boundary/targetHouse_northRoofEnd', '/Root/boundary/targetHouse_southRoofEnd', '/Root/boundary/targetHouse_eastUnderEaves', '/Root/boundary/targetHouse_northUnderEaves', '/Root/boundary/targetHouse_southUnderEaves', '/Root/boundary/targetHouse_westUnderEaves']

# create a new 'Slice'
slice1 = Slice(registrationName='Slice1', Input=pvfoam)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.Triangulatetheslice = 0
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [0.0001, 0.0, 0.44999998807907104]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [0.0, 0.0, 0.44999998807907104]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from slice1
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface With Edges'
slice1Display.ColorArrayName = ['POINTS', '']
slice1Display.SelectTCoordArray = 'None'
slice1Display.SelectNormalArray = 'None'
slice1Display.SelectTangentArray = 'None'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'None'
slice1Display.ScaleFactor = 0.17999999523162843
slice1Display.SelectScaleArray = 'None'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'None'
slice1Display.GaussianRadius = 0.008999999761581421
slice1Display.SetScaleArray = [None, '']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = [None, '']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.PolarAxes = 'PolarAxesRepresentation'
slice1Display.SelectInputVectors = [None, '']
slice1Display.WriteLog = ''

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface With Edges'
extractBlock1Display.ColorArrayName = ['POINTS', '']
extractBlock1Display.SelectTCoordArray = 'None'
extractBlock1Display.SelectNormalArray = 'None'
extractBlock1Display.SelectTangentArray = 'None'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = 0.17999999523162843
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = 0.008999999761581421
extractBlock1Display.SetScaleArray = [None, '']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = [None, '']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'
extractBlock1Display.SelectInputVectors = [None, '']
extractBlock1Display.WriteLog = ''

# ----------------------------------------------------------------
# restore active source
SetActiveSource(extractBlock1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
