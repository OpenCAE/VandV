# AIJ-PWEAB

Reproduction of benchmark test cases formulated in AIJ guidelines for practical applications of CFD to pedestrian wind environment around buildings

##  What is this?

This case solves benchmark test cases defined in Guidebook for CFD Predictions of Urban Wind Environment[E1] following the benchmark procedure.

## References
- [E1] Architectural Institute of Japan, Guidebook for CFD Predictions of Urban Wind Environment, 2020. URL: http://www.aij.or.jp/jpn/publish/cfdguide/index_e.htm

## 概要
都市の風環境予測のためのCFDガイドブック[J1]に掲載されているベンチマークテストケースをOpenFOAMを用いて解析し，風洞実験値と比較する．

## 参考文献
- [J1] 日本建築学会, 都市の風環境予測のためのCFDガイドブック, 2020. URL: http://www.aij.or.jp/jpn/publish/cfdguide/index.htm
- [J2] 計算工学ナビ URL:http://www.cenav.org/kdb/?p=1262

## Disclaimer:
OPENFOAM(R) is a registered trade mark of ESI Group,
the producer of the OpenFOAM software and owner of the OPENFOAM(R) trade marks.
This offering is not approved or endorsed by ESI Group.
