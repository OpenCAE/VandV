## Reproduction of benchmark test case F formulated in AIJ guidelines for practical applications of CFD to pedestrian wind environment around buildings

### Prerequisites

Python3, Gnuplot utility is required in order to run the case.

###  What is this?

This case solves benchmark test case A defined in AIJ guidelines for practical applications of CFD to pedestrian wind environment around a high-rise building [F1] following the benchmark procedure.

### Running the case
In order to run the case:

1. Type ./Allrun. This script will automatically make mesh, perform CFD calculation.
1. Type ./Allclean to clean up the case directory and reset to the initial state.

### References
- [F1] Architectural Institute of Japan, Guidebook for CFD Predictions of Urban Wind Environment, 2020. URL: http://www.aij.or.jp/jpn/publish/cfdguide/index_e.htm

## Disclaimer:
OPENFOAM(R) is a registered trade mark of ESI Group,
the producer of the OpenFOAM software and owner of the OPENFOAM(R) trade marks.
This offering is not approved or endorsed by ESI Group.
