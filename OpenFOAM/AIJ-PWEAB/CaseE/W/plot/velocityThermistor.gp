plot \
"< cat postProcessing/sample/*/measuringPoints_k_U.xy"\
  using ($0+1):(sqrt($5**2+$6**2+$7**2+2*$4)/URef) with lp title "CFD"\
,"exp/U.txt"\
  using ($0+1):2 with p pt 6 title "Exp"
#    EOF
