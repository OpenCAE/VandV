#!/usr/bin/env python3
import numpy as np
import subprocess

print("read x.dat, y.dat, z.dat")

x = np.loadtxt('x.dat')
y = np.loadtxt('y.dat')
z = np.loadtxt('z.dat')

print("x : ",x)
print("y : ",y)
print("z : ",z)

nx=len(x)
ny=len(y)
nz=len(z)

print("nx : ",nx)
print("ny : ",ny)
print("nz : ",nz)

print("make blockMeshDict")

file = open('system/blockMeshDict','w')
file.write("\
FoamFile\n\
{\n\
    version 2.0;\n\
    format ascii;\n\
    class dictionary;\n\
    object blockMeshDict;\n\
}\n\
scale 1;\n\
vertices\n\
(\n\
    (0 0 0) //0\n\
    (1 0 0) //1\n\
    (1 1 0) //2\n\
    (0 1 0) //3\n\
    (0 0 1) //4\n\
    (1 0 1) //5\n\
    (1 1 1) //6\n\
    (0 1 1) //7\n\
);\n\
blocks\n\
(\n\
    hex (0 1 2 3 4 5 6 7) ("+repr(nx-1)+" "+repr(ny-1)+" "+repr(nz-1)+") simpleGrading (1 1 1)\n\
);\n\
patches\n\
(\n\
    patch x_\n\
       ((0 4 7 3))\n\
    patch _x\n\
       ((2 6 5 1))\n\
    patch y_\n\
       ((1 5 4 0))\n\
    patch _y\n\
       ((3 7 6 2))\n\
    patch z_\n\
       ((0 3 2 1))\n\
    patch _z\n\
       ((4 5 6 7))\n\
);\n\
mergePatchPairs ();\n\
")
file.close()

print("run blockMesh")
subprocess.run("rm -rf constant/polyMesh/*", shell=True)

try:
   ret=subprocess.run("blockMesh -noFunctionObjects 2>&1 > log.blockMesh", shell=True, check=True)

except subprocess.CalledProcessError as e:
   print(e)
   exit(1)

#points
print("replace points")
file=open('constant/polyMesh/points','w')
file.write("\
FoamFile\n\
{\n\
    version 2.0;\n\
    format ascii;\n\
    class vectorField;\n\
    location \"constant/polyMesh\";\n\
    object points;\n\
}\n\
")
file.write("\n"+str(nx*ny*nz)+"\n(\n")
for k in range(nz): 
   for j in range(ny): 
      for i in range(nx): 
         vertices="("+str(x[i])+" "+str(y[j])+" "+str(z[k])+")\n"
         file.write(vertices)
file.write( ")\n" )
file.close()

print("End")
