#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import argparse
import math
import os

# constants
SMALL=1e-15
turbulentFiles=['k','epsilon','omega']

def parser():
    p = argparse.ArgumentParser()
    p.add_argument('-a','--windAngle',help='Wind angle in degree', type=float, default=0.0)
    p.add_argument('-w','--windDirection',help='Wind direction', type=str, default='')
    p.add_argument('inflowFilename')
    p.add_argument('-x','--xmin',help='Minimum x of boundary patch', type=float, default=0.0)
    p.add_argument('-X','--xmax', help='Maximum x of boundary patch', type=float, default=1.0)
    p.add_argument('-y','--ymin', help='Minimum y of boundary patch', type=float, default=0.0)
    p.add_argument('-Y','--ymax', help='Maximum y of boundary patch', type=float, default=1.0)
    p.add_argument('-d','--writeChangeDictionaryDict', help='Write changeDictionaryDict'
                       , action="store_true", default=False)
    p.add_argument('-z','--scalez', help='Scaling factor for z', type=float, default=1)
    p.add_argument('-U','--scaleU', help='Scaling factor for U', type=float, default=1)
    p.add_argument('-k','--scalek', help='Scaling factor for k', type=float, default=1)
    p.add_argument('-e','--scalee', help='Scaling factor for epsilon', type=float, default=1)
    p.add_argument('-o','--scaleo', help='Scaling factor for omega', type=float, default=1)
    p.add_argument('-C','--Cmu', help='Cmu', type=float, default=0.09)
    p.add_argument('-s','--skipHeader'
                       , help='Number of lines to skip at the beginning of the file', type=int, default=0)
    p.add_argument("--zAppendFirst", help='Append z at first', type=float, nargs='+')
    p.add_argument("--UAppendFirst", help='Append U at first', type=float, nargs='+')
    p.add_argument("--kAppendFirst", help='Append k at first', type=float, nargs='+')
    p.add_argument("--zAppendLast", help='Append z at last', type=float, nargs='+')
    p.add_argument("--UAppendLast", help='Append U at last', type=float, nargs='+')
    p.add_argument("--kAppendLast", help='Append k at last', type=float, nargs='+')
    p.add_argument('--zColumnNumber', help='Column number of z', type=int, default=1)
    p.add_argument('--UColumnNumber', help='Column number of U', type=int, default=2)
    p.add_argument('--kColumnNumber', help='Column number of k', type=int, default=3)
    p.add_argument('--eColumnNumber', help='Column number of epsilon', type=int, default=0)
    p.add_argument('--oColumnNumber', help='Column number of omega', type=int, default=0)
    return p.parse_args()


def getInflowData(args):
    # read inflow.csv
    inflow=np.genfromtxt(args.inflowFilename, delimiter=',', dtype='float', skip_header=args.skipHeader)

    # scaling
    z=inflow[:,args.zColumnNumber-1]*args.scalez
    U=inflow[:,args.UColumnNumber-1]*args.scaleU
    k=inflow[:,args.kColumnNumber-1]*args.scalek
    if args.eColumnNumber>0:
        epsilon=inflow[:,args.eColumnNumber-1]*args.scalee
    else:
        # estimate epsilon
        dUdz=np.gradient(U,z,edge_order=1)
        epsilon=math.sqrt(args.Cmu)*k*dUdz

    if args.oColumnNumber>0:
        omega=inflow[:,args.oColumnNumber-1]*args.scaleo
    else:
        # estimate omega
        omega=np.zeros(len(k))
        for i in range(len(k)):
            if k[i]>SMALL:
                omega[i]=epsilon[i]/(args.Cmu*k[i])

    return z,U,[k,epsilon,omega]


def getInletPatchNames(args):

    if args.windDirection != '':
        windDirections=("N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW")

        windDirectionsAngle = {key: float(i)*22.5 for i,key in enumerate(windDirections)}
        args.windAngle=windDirectionsAngle[args.windDirection]

    # Wind vector
    windVector = [
        -math.cos((90-float(args.windAngle))/180*math.pi),
        -math.sin((90-float(args.windAngle))/180*math.pi)
    ]

    # patchNames
    patchNames=[]
    slipPatcheNames=[]
    if windVector[0] > SMALL:
        patchNames.append("x_")
    elif windVector[0] < -SMALL:
        patchNames.append("_x")
    else:
        slipPatcheNames=["x_","_x"]
        windVector[0] = 0

    if windVector[1] > SMALL:
        patchNames.append("y_")
    elif windVector[1] < -SMALL:
        patchNames.append("_y")
    else:
        slipPatcheNames=["y_","_y"]
        windVector[1] = 0

    return patchNames, slipPatcheNames, windVector


def writeBoundaryData(args,patchNames,z,U,turbulent,windVector):
    for patchName in patchNames:
        writeBoundaryDataPatch(args,patchName,z,U,turbulent,windVector)


def writeBoundaryDataPatch(args,patchName,z,U,turbulent,windVector):
    
    # points
    path='constant/boundaryData'
    if not os.path.isdir(path):
        os.mkdir(path)
    path=path+'/'+patchName
    if not os.path.isdir(path):
        os.mkdir(path)
    pathTime=path+'/0'
    if not os.path.isdir(pathTime):
        os.mkdir(pathTime)
    file=open(path+'/points','w')
    file.write(repr(len(z)*2)+'\n(\n')
    xmin=args.xmin
    xmax=args.xmax
    ymin=args.ymin
    ymax=args.ymax
    if patchName == 'x_':
        xmax=args.xmin
    elif patchName == '_x':
        xmin=args.xmax
    elif patchName == 'y_':
        ymax=args.ymin
    elif patchName == '_y':
        ymin=args.ymax
    for ztmp in z:
        file.write('(%g %g %g)\n' % (xmin, ymin, ztmp))
    for ztmp in z:
        file.write('(%g %g %g)\n' % (xmax, ymax, ztmp))
    file.write(')\n')
    file.close()
    
    # U
    file=open(pathTime+'/U','w')
    file.write(repr(len(U)*2)+'\n')
    file.write('(\n')
    for n in range(2):
        for Utmp in U:
            file.write('(%g %g 0)\n' % (Utmp*windVector[0],Utmp*windVector[1]))
    file.write(')\n')
    file.close()
    
    # turbulent File
    for i in range(len(turbulentFiles)):
       file=open(pathTime+'/'+turbulentFiles[i],'w')
       file.write(repr(len(turbulent[i])*2)+'\n')
       file.write('(\n')
       for n in range(2):
           for tmp in turbulent[i]:
               file.write('%g\n' % (tmp))
       file.write(')\n')
       file.close()


def writeChangeDictionaryDict(patchNames,slipPatcheNames):
    file=open("system/changeDictionaryDict","w")
    
    # write header
    header = """\
FoamFile
{
    version 2.0;
    format ascii;
    class dictionary;
    object changeDictionaryDict;
}
"""
    file.write(header)

    for field in ['U','k','epsilon','omega','p']:
        file.write("\n\n%s\n{\n    boundaryField\n    {" % (field))
        if field == 'U':
            entry ="""\
            type            timeVaryingMappedFixedValue;
            setAverage      off;
            offset          (0 0 0);
            value           $internalField;
"""
        elif field == 'p':
            entry = """\
            type  zeroGradient;
"""
        else:
            entry ="""\
            type            timeVaryingMappedFixedValue;
            setAverage      off;
            offset          0;
            value           $internalField;
"""

        for patchName in patchNames:
            file.write('\n        %s\n        {\n%s        }' % (patchName, entry))

        for patchName in slipPatcheNames:
            file.write('\n        %s\n        {\n            type  slip;\n        }' % (patchName))

        file.write("\n    }\n}")

    file.write("\n")
    file.close()
    
if __name__ == '__main__':
    args=parser()

    z,U,turbulent = getInflowData(args)

    patchNames,slipPatcheNames,windVector = getInletPatchNames(args)

    writeBoundaryData(args, patchNames,z,U,turbulent,windVector)

    if args.writeChangeDictionaryDict:
        writeChangeDictionaryDict(patchNames,slipPatcheNames)
